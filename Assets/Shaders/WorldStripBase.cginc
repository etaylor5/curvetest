/*
    Properties
    {
        // world properties
		_WrldFreq           ("Frequency"            , Range(-10,10))    = 1.0
		_WrldAmplitude      ("Amplitude"            , Range(-10,10))    = 0.0
		_WrldOffset         ("Offset"               , Range(-10,10))    = 0.0
		_WrldCutoff         ("Cutoff Distance"      , Range(0,100))     = 10.0
		_WrldCutoffValue    ("Cutoff Value"         , Range(-100,100))  = 0.0
		_WrldHorExtent      ("Horizontal Extent"    , Range(-10,10))    = 6.0 
        _WrldHorAmplitude   ("Horizontal Amplitude" , Range(-10,10))    = 0.0
    }
*/

float _WrldFreq;
float _WrldAmplitude;
float _WrldOffset;
float _WrldCutoff;
float _WrldCutoffValue;
float _WrldHorExtent;
float _WrldHorAmplitude;

void vertWorldStripImpl(inout float4 vertPosLoc)
{
    const float TWOPI = 6.28318530718;

    // strip offset
    const float freq = _WrldFreq * TWOPI;
    float4 worldPos = mul(unity_ObjectToWorld, vertPosLoc);
    const float distance = worldPos.z + _WrldOffset;
    const float distOffset = _WrldAmplitude * ((cos(freq * distance) * 0.5) + 0.5);

    // horizontal offset
    const float horExtent = worldPos.x / _WrldHorExtent * TWOPI;
    const float horOffset = _WrldHorAmplitude * cos(horExtent);

    // cutoff offset
    const float cutOffset = _WrldCutoffValue * smoothstep(_WrldCutoff, _WrldCutoff + 1.0, worldPos.z) +
                            _WrldCutoffValue * (1.0 - smoothstep(-1.0, 0.0, worldPos.z));

    // compose
    worldPos.y += distOffset + horOffset + cutOffset;
    vertPosLoc = mul(unity_WorldToObject, worldPos);
}

// BEGIN wrappers
// vertBase implementation
#ifdef UNITY_STANDARD_CORE_FORWARD_INCLUDED

#if UNITY_STANDARD_SIMPLE
VertexOutputBaseSimple vertBase_wrldStrip(VertexInput v) {
    vertWorldStripImpl(v.vertex);
    return vertForwardBaseSimple(v);
}
VertexOutputForwardAddSimple vertAdd_wrldStrip(VertexInput v) {
    vertWorldStripImpl(v.vertex);
    return vertForwardAddSimple(v);
}
#else
VertexOutputForwardBase vertBase_wrldStrip(VertexInput v) {
    vertWorldStripImpl(v.vertex);
    return vertForwardBase(v);
}
VertexOutputForwardAdd vertAdd_wrldStrip(VertexInput v) {
    vertWorldStripImpl(v.vertex);
    return vertForwardAdd(v);
}
#endif

#endif

// vertShadowCaster implementation
#ifdef UNITY_STANDARD_SHADOW_INCLUDED

void vertShadowCaster_wrldStrip(VertexInput v
    , out float4 opos : SV_POSITION
#ifdef UNITY_STANDARD_USE_SHADOW_OUTPUT_STRUCT
    , out VertexOutputShadowCaster o
#endif
#ifdef UNITY_STANDARD_USE_STEREO_SHADOW_OUTPUT_STRUCT
    , out VertexOutputStereoShadowCaster os
#endif
)
{
    vertWorldStripImpl(v.vertex);

#if defined(UNITY_STANDARD_USE_SHADOW_OUTPUT_STRUCT) && defined(UNITY_STANDARD_USE_STEREO_SHADOW_OUTPUT_STRUCT)
    vertShadowCaster(v, opos, o, os);
#elif defined(UNITY_STANDARD_USE_SHADOW_OUTPUT_STRUCT)
    vertShadowCaster(v, opos, o);
#elif defined(UNITY_STANDARD_USE_STEREO_SHADOW_OUTPUT_STRUCT)
    vertShadowCaster(v, opos, os);
#else
    vertShadowCaster(v, opos);
#endif
}

#endif

// vertDeferred implementation
#ifdef UNITY_STANDARD_CORE_INCLUDED

VertexOutputDeferred vertDeferred_wrldStrip(VertexInput v)
{
    vertWorldStripImpl(v.vertex);
    return vertDeferred(v);
}

#endif

// vert_meta implementation
#ifdef UNITY_STANDARD_META_INCLUDED

v2f_meta vert_meta_wrldStrip(VertexInput v)
{
    vertWorldStripImpl(v.vertex);
    return vert_meta(v);
}

#endif

// surface implementation
#ifdef UNITY_CG_INCLUDED

void vertWorldStrip_base(inout appdata_base v)
{
    vertWorldStripImpl(v.vertex);
}

void vertWorldStrip_full(inout appdata_full v)
{
    vertWorldStripImpl(v.vertex);
}

void vertWorldStrip_tan(inout appdata_tan v)
{
    vertWorldStripImpl(v.vertex);
}

#endif
// END wrappers