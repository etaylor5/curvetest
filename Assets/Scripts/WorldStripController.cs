﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct WorldStripSettings
{
    public float Frequency;
    public float Amplitude;
    public float Offset;
    public float Cutoff;
    public float CutoffValue;
    public float HorExtent;
    public float HorAmplitude;
}

/// <summary>
/// Manages a world strip's basic parameters and logic.
/// Should be added to a root object in the scene hierarchy that holds
/// all of the objects in the strip.
/// </summary>
[ExecuteAlways]
#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class WorldStripController : MonoBehaviour
{
    [SerializeField] private WorldStripSettings WorldSettings = new WorldStripSettings();
    [SerializeField] private Vector3 ChildBoundsOverride = new Vector3(10.0f, 10.0f, 10.0f);

#if UNITY_EDITOR
    WorldStripController()
    {
        EditorApplication.playModeStateChanged -= OnPlaymodeChanged;
        EditorApplication.playModeStateChanged += OnPlaymodeChanged;
    }

    void OnPlaymodeChanged(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingEditMode || state == PlayModeStateChange.EnteredEditMode)
            EnableFlat();
    }

    void Reset()
    {
        Init();
    }

    void OnValidate()
    {
        Init();
    }

    void OnDestroy()
    {
        EditorApplication.playModeStateChanged -= OnPlaymodeChanged;
    }
#endif

    void Start()
    {
        Init();
    }

#if UNITY_EDITOR
    public void ToggleFlat()
    {
        isFlat = !isFlat;

        InitWorldMaterials(isFlat ? FlatSettings : WorldSettings);
        InitWorldStripObjects(isFlat ? FlatSettings : WorldSettings);
    }

    public void EnableFlat()
    {
        isFlat = false;
        ToggleFlat();
    }
#endif

    public void Init()
    {
#if UNITY_EDITOR
        EnableFlat();
#endif
        GatherChildren();
        FixCulling();
        InitWorldStripObjects(WorldSettings);
        GatherWorldMaterials();
        InitWorldMaterials(WorldSettings);
#if UNITY_EDITOR
        isFlat = false;
#endif
    }

    private void InitWorldStripObjects(WorldStripSettings settings)
    {
        foreach (var obj in ChildrenWorldObjects)
        {
            obj.Cfg = settings;
        }
    }

    private void InitWorldMaterials(WorldStripSettings settings)
    {
        foreach(var mat in WorldStripMaterials)
        {
            InitWorldMaterial(mat, settings);
        }
    }

    private void InitWorldMaterial(Material target, WorldStripSettings settings)
    {
        target.SetFloat("_WrldFreq", settings.Frequency);
        target.SetFloat("_WrldAmplitude", settings.Amplitude);
        target.SetFloat("_WrldOffset", settings.Offset);
        target.SetFloat("_WrldCutoff", settings.Cutoff);
        target.SetFloat("_WrldCutoffValue", settings.CutoffValue);
        target.SetFloat("_WrldHorExtent", settings.HorExtent);
        target.SetFloat("_WrldHorAmplitude", settings.HorAmplitude);
    }

    private void GatherWorldMaterials()
    {
        WorldStripMaterials.Clear();
        
        foreach(var child in ChildrenWorldShader)
        {
            var meshRenderer = child.GetComponent<MeshRenderer>();

            foreach(var mat in meshRenderer.sharedMaterials)
            {
                if (WorldStripMaterials.Contains(mat))
                    continue;

                WorldStripMaterials.Add(mat);
            }
        }
    }

    /// <summary>
    /// Populates local lists of children on which to apply the WorldStrip shader
    /// and that have a WorldStripObject component.
    /// </summary>
    private void GatherChildren()
    {
        ChildrenWorldObjects.Clear();
        ChildrenWorldShader.Clear();

        var traversal = new List<Transform>();
        traversal.Add(this.transform);
        for (int i = 0; i < traversal.Count; ++i)
        {
            foreach (Transform child in traversal[i])
            {
                traversal.Add(child);
            }
        }

        foreach(var child in traversal)
        {
            var wsObject = child.GetComponent<WorldStripObject>();

            if(wsObject != null)
            {
                ChildrenWorldObjects.Add(wsObject);
                continue;
            }

            var meshFilter = child.GetComponent<MeshFilter>();
            var meshRenderer = child.GetComponent<MeshRenderer>();

            if (meshFilter != null)
            {
                ChildrenWorldShader.Add(meshFilter);
            }
        }
    }

    /// <summary>
    /// Expands bounding boxes of the strip objects as a workaround for
    /// Unity's frustum culling.
    /// </summary>
    private void FixCulling()
    {
        var meshes = new HashSet<Mesh>();
        foreach (var child in ChildrenWorldShader)
        {        
            meshes.Add(child.sharedMesh);
        }

        foreach (var mesh in meshes)
        {
            var bounds = mesh.bounds;
            bounds.SetMinMax(
                bounds.min - ChildBoundsOverride,
                bounds.max + ChildBoundsOverride
                );
            mesh.bounds = bounds;
        }
    }

    private List<MeshFilter>        ChildrenWorldShader = new List<MeshFilter>();
    private List<WorldStripObject>  ChildrenWorldObjects = new List<WorldStripObject>();
    private HashSet<Material>       WorldStripMaterials = new HashSet<Material>();
#if UNITY_EDITOR
    private bool isFlat = false;
#endif

    private readonly WorldStripSettings FlatSettings = new WorldStripSettings()
    {
        Frequency = 1.0f,
        Amplitude = 0.0f,
        Offset = 0.0f,
        Cutoff = 9999.0f,
        CutoffValue = 0.0f,
        HorExtent = 9999.0f,
        HorAmplitude = 0.0f
    };
}

#if UNITY_EDITOR
[CustomEditor(typeof(WorldStripController))]
public class WorldStripControllerEditor : Editor
{
    SerializedProperty WorldSettings;
    SerializedProperty EditorShader;

    void OnEnable()
    {
        WorldSettings = serializedObject.FindProperty("WorldSettings");
        EditorShader = serializedObject.FindProperty("EditorShader");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update();
        serializedObject.ApplyModifiedProperties();

        var ctrl = (WorldStripController)target;
        if (GUILayout.Button("Toggle Flat Mode"))
        {
            ctrl.ToggleFlat();
        }

        var style = new GUIStyle();
        style.richText = true;
        GUILayout.Label("<b><color=red>WARNING:</color>\nOnly edit and save strip objects\nwhile flat mode is ON</b>", style);

        if (GUILayout.Button("Reinitialize"))
        {
            ctrl.Init();
        }
    }
}
#endif