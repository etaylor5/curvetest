﻿using System;
using UnityEngine;

/// <summary>
/// CPU-based, updates an object's position to match the warping of a
/// world strip.
/// </summary>
[ExecuteAlways]
public class WorldStripObject : MonoBehaviour
{
    [NonSerialized] public WorldStripSettings Cfg;

    void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        // strip offset
        float freq = Cfg.Frequency * 2.0f * Mathf.PI;
        Vector3 worldPos = transform.position;
        float distance = worldPos.z + Cfg.Offset;
        float distOffset = Cfg.Amplitude * ((Mathf.Cos(freq * distance) * 0.5f) + 0.5f);

        // horizontal offset
        float horExtent = worldPos.x / Cfg.HorExtent * 2.0f * Mathf.PI;
        float horOffset = Cfg.HorAmplitude * Mathf.Cos(horExtent);

        // cutoff offset
        float cutOffset = (worldPos.z > Cfg.Cutoff || worldPos.z < 0.0f) ? Cfg.CutoffValue : 0.0f;

        Vector3 offset = new Vector3(
            0.0f,
            distOffset + horOffset + cutOffset,
            0.0f);

        transform.position = transform.position + offset - lastOffset;

        lastOffset = offset;
    }

    private Vector3 lastOffset = Vector3.zero;
}
