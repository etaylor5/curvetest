﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DemoScrollable : MonoBehaviour, IDragHandler
{
    [SerializeField] private Transform Target = null;
    [SerializeField] private float Multiplier = 1.0f;

    [SerializeField] private float MinPos = -20.0f;
    [SerializeField] private float MaxPos = 0.0f;

    public void OnDrag(PointerEventData eventData)
    {
        var newPos = Target.localPosition + Vector3.forward * Multiplier * Time.deltaTime * eventData.delta.y;

        newPos.z = Mathf.Min(newPos.z, MaxPos);
        newPos.z = Mathf.Max(newPos.z, MinPos);

        Target.localPosition = newPos;
    }
}
