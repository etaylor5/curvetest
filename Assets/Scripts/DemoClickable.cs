﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider))]
public class DemoClickable : MonoBehaviour
{
    public void OnMouseDown()
    {
        Debug.Log("Click down!");
    }

    public void OnMouseUp()
    {
        Debug.Log("Click up!");
    }
}
