﻿using UnityEngine;

/// <summary>
/// CPU-based billboard component.
/// Tracks and orients attached object toward a user-defined transform.
/// </summary>
public class Billboard : MonoBehaviour
{
    [SerializeField] private Transform TrackedObj = null;
    [SerializeField] private Vector3   LocalFwd   = Vector3.forward;
    [SerializeField] private Vector3   LocalUp    = Vector3.up;

    void Start()
    {
        if(TrackedObj == null)
        {
            TrackedObj = Camera.main.transform;
        }
    }
 
    void Update()
    {
        Vector3 wrldToDir = (TrackedObj.position - transform.position).normalized;
        Vector3 wrldFwdDir = transform.rotation * LocalFwd;

        transform.rotation = Quaternion.FromToRotation(wrldFwdDir, wrldToDir) * transform.rotation;

        Vector3 wrldUpDir = transform.rotation * LocalUp;
        Vector3 targetUp = Vector3.Cross(Vector3.Cross(wrldToDir, Vector3.up).normalized, wrldToDir);

        transform.rotation = Quaternion.FromToRotation(wrldUpDir, targetUp) * transform.rotation;
    }
}
